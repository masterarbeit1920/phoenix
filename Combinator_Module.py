import math, time, random, functools
from Helper_Methods import *


class Combinator_Module:

    '''
    - initializes all necessary variables
    - generate 'not_done_connections'
    '''
    def __init__(self, team_amount, match_size, intensity = 100, match_generation_method='random', compare_method ='more_low'):
        self.team_amount = team_amount
        self.teams = list(range(team_amount))
        self.match_size = match_size
        self.intensity = intensity
        if match_generation_method not in ['random','participation']:
            raise ValueError('match_generation_method can either be "random" or "participation"')
        self.match_generation_method = match_generation_method
        if compare_method not in ['more_low', 'less_high']:
            raise ValueError('compare_option can either be "more_low" or "less_high"')
        self.compare_method = compare_method

    '''
    - initialize 'match_combo' with generic first match or 'known_matches'
    - delete created connections from 'not_done_connections' 
    '''
    def initialise_match_combo(self, known_matches):
        match_combo = []
        if known_matches:
            match_combo += list(map(lambda match: sorted(match), known_matches))
        else:
            first_match = list(self.teams[:self.match_size])
            match_combo += [sorted(first_match)]
        for match in match_combo:
            self.remove_done_connections(match)
        return match_combo

    '''
    function which is accessed from the outside
    - initialize 'match_combo' with generic first match or 'known_matches' and update 'not_done_connections'
    - print information
    - calls 'generate_matches'
    '''
    def get_schedule(self, known_matches=None, meta_information= False):
        self.not_done_connections = combinationsFromArray(self.teams, 2)
        self.match_combo = self.initialise_match_combo(known_matches)

        start = time.time()
        self.generate_matches()
        elapsed_time = time.time() - start
        if meta_information:
            self.print_job_result(elapsed_time)
        return self.match_combo

    '''
    - creates and appends matches as long as 'not_done_connections' is not empty
    - updates 'match_combo' and 'not_done_connections'
    '''
    def generate_matches(self):
        while len(self.not_done_connections) > 0:
            next_match = self.get_next_match()
            self.remove_done_connections(next_match)
            self.match_combo.append(sorted(next_match))

    def print_settings(self):
        print('*** Settings ***')
        print('amount of teams:', self.team_amount)
        print('teams per match:', self.match_size)
        print('intensity:', self.intensity)
        print('pay attention to participation:', self.match_generation_method)
        print('compare method:', self.compare_method)
        print('****************')

    def print_prognoses(self):
        amount_of_connections = (self.team_amount * (self.team_amount - 1) / 2)
        match_connections = (self.match_size * (self.match_size - 1) / 2)
        schedule_lower_bound = math.ceil(amount_of_connections / float(match_connections))
        print('*** Prognoses ***')
        print('connections to create:',int(amount_of_connections))
        print('schedule size lower bound:',schedule_lower_bound)
        print('schedule size upper bound:', int(amount_of_connections))
        print('*****************')

    def print_job_result(self,elapsed_time):
        print('*** Meta Information ***')
        print('elapsed time:', "%.2f" % (elapsed_time * 1000.),'ms')
        print('Match amount:', len(self.match_combo))
        print('player participation distribution:', get_participation_distribution(self.match_combo))
        connections_sorted_by_activity = get_sorted_connections_by_activity(self.match_combo)
        for key,value in connections_sorted_by_activity.items():
            connections_sorted_by_activity[key] = len(value)
        print('connection activation distribution:', connections_sorted_by_activity)
        print('************************')

    '''
    - removes connections which are created by a match from 'not_done_connections'
    '''
    def remove_done_connections(self, match):
        done_connections = combinationsFromArray(match, 2)
        new_not_done_connections = list(
            filter(lambda connection: connection not in done_connections, self.not_done_connections))
        self.not_done_connections = new_not_done_connections

    '''
    - generates a number of matches (depending on 'intensity') and pics the best
    - how match is generate depends on 'attention_to_participation'
    - first connection from 'not_done_connections' has to be included -> 'next_connection'
    - the best match is chosen an the option 'compare_option'
    - for 'more_less' the distribution is simply compared
    - where as for 'less_high' the distribution is reversed and all values are made negative 
    '''
    def get_next_match(self):
        connections_with_global_activity = get_counted_connection_as_dict(self.match_combo + [self.teams])
        match_generating_method, teams = self.get_match_generating_method_and_teams_formatted()
        next_connection = self.get_next_connection_to_create()

        best_connection_distribution = None
        match = None
        i = 0

        while i < self.intensity or match is None:
            next_match_to_test = match_generating_method(next_connection,teams)
            connection_distribution = self.get_distribution_values_of_new_match(next_match_to_test,connections_with_global_activity)
            if self.compare_method == 'less_high':
                connection_distribution = list(map(lambda connection: - connection, list(reversed(connection_distribution))))
            if best_connection_distribution is None or self.is_new_option_better(best_connection_distribution,connection_distribution):
                match = next_match_to_test
                best_connection_distribution = connection_distribution
            i += 1
        return match

    '''
    - compares the 'old' array to the 'new' array
    - if the 'new' array has higher values this function returns true otherwise false 
    '''
    def is_new_option_better(self,old,new):
        array = [(False,old),(True,new)]
        return max(array,key=(lambda entry: entry[1]))[0]

    '''
    - gets the distribution of the current match in comparison with the overall distribution of connection activities
    '''
    def get_distribution_values_of_new_match(self,match,connections_with_global_activity):
        distribution_of_connections = get_distribution_of_match(match, connections_with_global_activity)
        return list(distribution_of_connections.values())

    '''
    - creates match which includes 'next_connection'
    - other teams are picked depending on there previous participation
    '''
    def generate_random_match_participation(self,next_connection, sorted_team_options):
        additional_teams = []
        i = 0
        while len(additional_teams) < self.match_size - 2:
            teams_on_current_level = list(set(sorted_team_options[i]) - set(next_connection))
            still_needed_teams = self.match_size - len(additional_teams) - 2
            additional_teams += random.sample(teams_on_current_level,
                                              min(len(teams_on_current_level), still_needed_teams))
            i += 1
        return list(next_connection) + additional_teams

    '''
    - creates match which includes 'next_connection'
    - other teams are picked randomly
    '''
    def generate_random_match(self, next_connection, teams):
        suffix_options = list(set(teams)-set(next_connection))
        return list(next_connection) + random.sample(suffix_options, self.match_size - 2)
    '''
    - returns first connection of 'not_done_connections'
    '''
    def get_next_connection_to_create(self):
        return sorted(self.not_done_connections)[0]

    '''
    - pics match generating method depending on 'attention_to_participation' 
    - sorts lists of teams if teams are picked depending on there participation
    '''
    #TODO A different Algorithm of finding teams for the new match can be found and here placed - right now only random and attention on particiaption is implemented
    def get_match_generating_method_and_teams_formatted(self):
        match_generating_method = self.generate_random_match
        teams = self.teams
        if self.match_generation_method == 'participation':
            match_generating_method = self.generate_random_match_participation
            teams = self.get_teams_sorted_by_participation()
        return match_generating_method, teams

    '''
    - sort teams by there participation
    '''
    def get_teams_sorted_by_participation(self):
        involved_teams = functools.reduce(lambda a, b: a + b, self.match_combo) + self.teams
        unique, counts = numpy.unique(involved_teams, return_counts=True)

        sorted_ = []
        minimum = numpy.array(counts).min()
        for i in range(minimum, numpy.array(counts).max() + 1):
            sorted_.append([])

        for i in range(len(unique)):
            sorted_[counts[i] - minimum].append(unique[i])

        return sorted_
