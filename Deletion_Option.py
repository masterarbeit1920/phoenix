from Helper_Methods import get_distribution_of_match


class Deletion_Option:
    '''
    - initialize 'index' and 'match' and the 'connections_with_global_activity' dict
    '''
    def __init__(self, index,match,connections_with_global_activity):
        self.index = index
        self.match = match
        self.connections_with_global_activity = connections_with_global_activity

    '''
    - the distribution of the connection included in the current match in perspective to all matches is generated
    - the amounts are then returned as kind of quality representation of that deletion option
    '''
    #TODO here a different quality metrics can be found
    def get_quality_of_deletion_option(self):
        distribution_of_connections = get_distribution_of_match(self.match,
                                                                self.connections_with_global_activity)
        return list(reversed(list(distribution_of_connections.values())))