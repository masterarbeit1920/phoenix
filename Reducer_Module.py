import random, time

from Deletion_Option import Deletion_Option
from Merging_Option import Merge_Option
from Helper_Methods import *


class Reducer_Module:
    '''
    - initializes 'team_amount', 'teams_per_match', 'match_combo_orig', 'teams_per_match', 'merge_intensity', 'fill_method' and 'deletion_method', 'blocked_matches'
    '''
    def __init__(self,
                 team_amount,
                 match_size,
                 schedule,
                 known_matches = None,
                 merge_intensity = 20,
                 fill_method ='block',
                 deletion_method ='highest',
                 merge_method='less_high'):
        self.match_combo_orig = schedule.copy()
        self.match_combo = None
        if known_matches:
            self.blocked_match_indices = self.get_indices_of_matches(known_matches)
        else:
            self.blocked_match_indices = []
        self.intensity = merge_intensity
        self.initialize_user_options(fill_method,deletion_method,merge_method)
        self.team_amount = team_amount
        self.match_size = match_size


    def initialize_user_options(self,fill_method,deletion_method,merge_method):
        self.raise_error_when_not_registered_option_is_chosen('deletion_method',deletion_method,['highest','random'])
        self.deletion_method = deletion_method

        self.raise_error_when_not_registered_option_is_chosen('fill_method', fill_method, ['block', 'random', 'first'])
        self.fill_method = fill_method

        self.raise_error_when_not_registered_option_is_chosen('merge_method', merge_method, ['more_low','less_high'])
        self.merge_method = merge_method

    def raise_error_when_not_registered_option_is_chosen(self,method_name, chosen_option, registered_options):
        if chosen_option not in registered_options:
            raise ValueError(method_name,'has to be from',registered_options)


    '''
    - gets the indeces of a list of matches in reference to the list of matches 'match_combo_orig'
    '''
    def get_indices_of_matches(self, matches):
        matches_indices = []
        matches_strings = list(map(lambda match: str(match), matches))
        reference_matches_strings = list(map(lambda match: str(match), self.match_combo_orig))
        for i,match_str in enumerate(reference_matches_strings):
            if match_str in matches_strings:
                matches_indices.append(i)
        return matches_indices

    def print_settings(self):
        print('*** Settings ***')
        print('Amount of matches:', len(self.match_combo_orig))
        print('team amount:',self.team_amount)
        print('teams per match:', self.match_size)
        print('intensity:', self.intensity)
        print('fill method:', self.fill_method)
        print('deletion method:', self.deletion_method)
        print('merge method:', self.merge_method)
        print('****************')

    '''
    - delete matches which have no single connection in them
    - merge as many matches as possible together 
    - returns the modified combination of matches
    '''
    def get_reduced_schedule(self, show_step_improvements= False, meta_information=False):
        self.match_combo = self.match_combo_orig.copy()

        start_deletion = time.time()
        match_amount_before_deletion = len(self.match_combo)
        self.delete_matches_without_single_connections(show_step_improvements)
        deleted_matches = match_amount_before_deletion - len(self.match_combo)
        elapsed_deletion = time.time() - start_deletion

        start_merging = time.time()
        match_amount_before_merging = len(self.match_combo)
        self.merge_matches(show_step_improvements)
        merged_matches = match_amount_before_merging - len(self.match_combo)
        elapsed_merging = time.time() - start_merging

        if show_step_improvements:
            print('Final:')
            self.print_step_improvements()

        if meta_information:
            self.print_meta_information(elapsed_deletion,elapsed_merging,deleted_matches,merged_matches)
        

        return self.match_combo

    def print_meta_information(self, elapsed_deletion, elapsed_merging, deleted_matches, merged_matches):
        print('\n*** Meta Information ***')
        print('deletion elapsed time:', "%.2f" % (elapsed_deletion * 1000.), 'ms')
        print('deleted matches:',deleted_matches)
        print('merging elapsed time:', "%.2f" % (elapsed_merging * 1000.), 'ms')
        print('merged matches:', merged_matches)
        print('final amount of matches:', len(self.match_combo))
        print('player participation distribution:', get_participation_distribution(self.match_combo))
        connections_sorted_by_activity = get_sorted_connections_by_activity(self.match_combo)
        for key, value in connections_sorted_by_activity.items():
            connections_sorted_by_activity[key] = len(value)
        print('connection activation distribution:', connections_sorted_by_activity)
        print('************************')

    def print_step_improvements(self):
        print(len(self.match_combo), list(map(lambda activity: (activity,
                                                                len(self.connections_sorted_by_activity[activity])),
                                              self.connections_sorted_by_activity.keys())))

    '''
    - filter those matches out which can be deleted - because they dont have any single connections - 'get_matches_without_single_connections'
    - pic those match which is the most suitable for deletion - according to 'get_quality_of ...' method - 'chosen_deletion'
    - delete this match
    - reevaluate the connection amounts and repeat process until no match can deleted any more - 'update_connection_activity_dict()'    
    '''
    #TODO here it can be optimized how matches are chosen for deletion - currently those matches with the highest connection acitivities are prefered to deleted
    def delete_matches_without_single_connections(self,show_step_improvements):
        if show_step_improvements:
            print('Deletion:')
        is_first_run = True
        while True:
            self.update_connection_activity_dict()
            if show_step_improvements and not is_first_run:
                self.print_step_improvements()
            is_first_run = False
            matches_without_single_connections = self.get_matches_without_single_connections()
            if not matches_without_single_connections:
                return
            if self.deletion_method == 'random':
                chosen_deletion = random.choice(matches_without_single_connections)
            elif self.deletion_method == 'highest':
                chosen_deletion = max(matches_without_single_connections, key=(lambda option: option.get_quality_of_deletion_option()))
            self.match_combo.pop(chosen_deletion.index)

    '''
    - find a number of pairs of matches which can be merged - 'merge_options'
    - pick the best merge_option - 'chosen_merge_option' - according to the 'get_quality...' method
    - 'conduct_merging_of_two_matches'
    - reevaluate the appearing connection in all matches and repeat process until no match can be deleted any more
    '''
    def merge_matches(self, show_step_improvements):
        if show_step_improvements:
            print('Merging:')
        is_first_run = True
        while True:
            self.update_connection_activity_dict()
            if show_step_improvements and not is_first_run:
                self.print_step_improvements()
            is_first_run = False
            merge_options = self.get_merge_opportunities()
            if not merge_options:
                return
            chosen_merge_option = min(merge_options, key=(lambda option: option.get_quality_of_merge_option()))
            self.conduct_merging_of_two_matches(chosen_merge_option)

    '''
    - overrides 'connection_activity_dict' with current connection information
    - reevaluates the amount every connection is appearing in all matches overall
    '''
    def update_connection_activity_dict(self):
        self.connections_sorted_by_activity = get_sorted_connections_by_activity(self.match_combo)
        self.connections_with_global_activity = get_counted_connection_as_dict(self.match_combo)

    '''
    - goes to every match - if a match has no single connection and is not a blocked match it is added as 'deletion_option'
    - the list of options is then returned
    '''
    def get_matches_without_single_connections(self):
        matches_without_single_connections = []
        for i, match in enumerate(self.match_combo):
            amount_of_single_connections = len(get_connections_with_given_activity(self.connections_sorted_by_activity, match, 1))
            if i not in self.blocked_match_indices and amount_of_single_connections == 0:
                matches_without_single_connections.append(Deletion_Option(i,match,self.connections_with_global_activity))
        return matches_without_single_connections if len(matches_without_single_connections) > 0 else None

    '''
    - a list of random pairs of indices (representing the matches in 'self.match_combo') is created 
    - according to the list of indices pairs different 'merge_option' are evaluated
    - if a 'merge_option' is valid it is added to a list which is eventually returned
    '''
    #Todo it could be implemented that the loop looks as long as a valid merge opportunity was found - but has to have a upper boundary!
    #Todo the set of matches which is possible for matching can be reduced here!
    def get_merge_opportunities(self):
        merge_opportunities = []
        random_index_pairs = self.get_list_of_random_indice_pairs()
        for pair in random_index_pairs:
            merge_option = Merge_Option(self.team_amount,
                                        self.match_size,
                                        pair,
                                        self.match_combo,
                                        self.connections_sorted_by_activity,
                                        self.connections_with_global_activity,
                                        self.fill_method,
                                        self.merge_method)
            if merge_option.does_musthave_teams_fit_into_match_size():
                merge_opportunities.append(merge_option)
        return merge_opportunities if len(merge_opportunities) > 0 else None

    '''
    - 'amount' - amount of random indece pairs
    - 'range_' - range from which the indices should come
    - get indeces of not blocked matches
    - check if the 'amount' is higher then the actual amount of combinations in the 'range_'   
    - if there are lesser combinations - return all combination of indices of 'range_
    - if there are more combinations - generate 'amount' of random indices from 'range_' 
    - no duplicated pairs are allowed
    '''
    def get_list_of_random_indice_pairs(self):
        indeces = list(set(list(range(len(self.match_combo)))) - set(self.blocked_match_indices))
        logical_maximum_amount_of_combination = len(indeces) * (len(indeces) - 1) / 2
        if logical_maximum_amount_of_combination < self.intensity:
            return combinationsFromArray(indeces, 2)
        else:
            reference_list = []
            list_ = []
            while len(list_) < self.intensity:
                new_pair = sorted(random.sample(indeces, 2))
                if str(new_pair) not in reference_list:
                    reference_list.append(str(new_pair))
                    list_.append(new_pair)
            return list_

    '''
    - delete according to the given 'merge_option' two matches
    - add the 'merged_match' according to the given 'merge_option'
    '''
    def conduct_merging_of_two_matches(self, merge_option):
        new_match_combination = []
        for i in range(len(self.match_combo)):
            if i not in merge_option.index_pair:
                new_match_combination.append(self.match_combo[i])
        new_match_combination.append(merge_option.merged_match)
        self.match_combo = new_match_combination
