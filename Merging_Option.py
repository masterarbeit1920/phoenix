from Helper_Methods import *
import random

class Merge_Option:
    '''
    - initialize the input values   'index_pair',
                                    'match_pair',
                                    'connection_activity_dict',
                                    'connections_with_global_activity',
                                    'teams_per_match'
                                    'merged_match'
                                    'fill_option
    - 'prepare_merged_match'
    - check if 'merged_match' is already present in the match_combo
    '''
    def __init__(self, team_amount, match_size, index_pair, match_combo, connection_activity_dict, connections_with_global_activity, fill_method, merge_method):
        self.index_pair = index_pair
        self.match_pair = [match_combo[index_pair[0]], match_combo[index_pair[1]]]
        self.connections_sorted_for_activity = connection_activity_dict
        self.connections_with_global_activity = connections_with_global_activity
        self.match_size = match_size
        self.team_amount = team_amount
        self.fill_method = fill_method
        self.merge_method = merge_method
        self.prepare_merged_match()
        self.is_valid = self.is_merged_match_not_present_in_match_combo(match_combo)

    '''
    - convert 'match_combo' in list of str for comparison
    - remove 'merging_matches' from that list
    - check if 'merged_match' is part of that list
    '''
    def is_merged_match_not_present_in_match_combo(self, match_combo):
        match_combo_str = list(map(lambda match: str(match), match_combo))
        merging_matches_str = list(map(lambda match: str(match),self.match_pair))
        match_combo_str_without_merging_matches = list(set(match_combo_str) - set(merging_matches_str))
        return str(self.merged_match) not in match_combo_str_without_merging_matches

    '''
    - determines the teams which have to be included when merging the matches 'merged_match_teams'
    - if they fit into a match
    - assemble 'merged_match'
    '''
    def prepare_merged_match(self):
        self.merged_match_teams = self.get_teams_which_have_to_included_when_merging_pair()
        self.merged_match = None
        if self.does_musthave_teams_fit_into_match_size():
            self.merged_match = self.assemble_merged_match()

    '''
    - determine the teams which are involved in single connection in the pair of matches
    - determine the teams which are involved in the double connection which both matches HAVE IN COMMON
    - return list of teams which would need to be included in 'merged_match'
    '''
    def get_teams_which_have_to_included_when_merging_pair(self):
        single_connection_teams = self.get_single_connections_of_pair_of_matches()
        overlapping_double_connections = self.get_teams_involved_in_overlapping_double_connections()
        return list(set(single_connection_teams + overlapping_double_connections))

    '''
    - do the same thing for the first and second match of the pair
    - get the connections of the match which appear globally only once - 'single_one' and 'single_two'
    - extract the included teams of those connections - 'single_teams_one' and 'single_teams_two'
    - delete duplicated teams and return list
    '''
    def get_single_connections_of_pair_of_matches(self):
        single_one = get_connections_with_given_activity(self.connections_sorted_for_activity, self.match_pair[0], 1)
        single_teams_one = functools.reduce(lambda a, b: a + b, single_one) if single_one else []
        single_two = get_connections_with_given_activity(self.connections_sorted_for_activity, self.match_pair[1], 1)
        single_teams_two = functools.reduce(lambda a, b: a + b, single_two) if single_two else []
        return list(set(single_teams_one + single_teams_two))

    '''
    - get the double connection of both matches as string- 'first' and 'second'
    - find those double connection which are in both matches the same
    - return the corresponding teams of those double connections
    '''
    def get_teams_involved_in_overlapping_double_connections(self):
        first = get_connections_with_given_activity(self.connections_sorted_for_activity, self.match_pair[0], 2, asString=True)
        second = get_connections_with_given_activity(self.connections_sorted_for_activity, self.match_pair[1], 2,
                                                     asString=True)
        overlapping = list(
            map(lambda connection: get_connection_from_string(connection), list(set(first) & set(second))))
        if overlapping:
            return functools.reduce(lambda a, b: a + b, overlapping)
        else:
            return []

    '''
    - checks if the must have teams fit into the amount of teams which are allowed in a match
    '''
    def does_musthave_teams_fit_into_match_size(self):
        return len(self.merged_match_teams) <= self.match_size

    '''
    - the amount of teams which are still missing to a complete match 'open_team_slots' are determined
    - if there are no teams missing - 'merged_match_teams' is returned as _merged_match
    - if not the teams which can be used to fill up 'open_team_slots' are determined -> 'team_possible_for_slots'
    - if there are exactly that many teams possible as there are slots - there are simply used
    - if not the remaining teams are filtered until the right amount of teams is remaining which is then be used 
    '''
    def assemble_merged_match(self):
        open_team_slots = self.match_size - len(self.merged_match_teams)
        if open_team_slots > 0:
            team_possible_for_slots = list(set(list(range(self.team_amount))) - set(self.merged_match_teams))
            if open_team_slots == len(team_possible_for_slots):
                self.merged_match_teams += team_possible_for_slots
            else:
                if self.fill_method == 'block':
                    self.merged_match_teams += self.fill_remaining_team_slots_best_as_possible(open_team_slots,
                                                                                      team_possible_for_slots)
                elif self.fill_method == 'random':
                    self.merged_match_teams += random.sample(team_possible_for_slots,open_team_slots)

                elif self.fill_method == 'first':
                    self.merged_match_teams += team_possible_for_slots[:open_team_slots]

        merged_match = list(sorted(self.merged_match_teams))
        return merged_match

    '''
    - amount of teams which need to be removed is determined - 'amount_teams_need_to_blocked'
    - the appearing connections are walked through (from often appearing to seldom appearing) 
    - it is tried to avoid the creation of the current connection by blocking the involved teams
    - therefore it is looked if one of the teams is already blocked or is necessary for the new match and therefore the other one need or is not needed to be blocked
    - if there are enough teams blocked those teams are removed and the remaining are returned
    - if there are still more teams after going to all connections then necessary the first few are chosen and returned
    '''
    #Todo Instead of block teams and taking the remaining once for filling the teams slots a different approach for filling the teams slots can be found
    #TODO Instead of just taking the first teams which are not blocked yet if there are two much - chose more clever
    def fill_remaining_team_slots_best_as_possible(self, open_team_slots, team_possible_for_slots):
        amount_teams_need_to_blocked = len(team_possible_for_slots) - open_team_slots
        to_block = set()
        for activity in reversed(list(self.connections_sorted_for_activity.keys())):
            for connection_string in self.connections_sorted_for_activity[activity]:
                team_to_block = self.get_team_to_block_to_avoiding_current_connection(connection_string, to_block)
                if team_to_block:
                    to_block.add(team_to_block)
                if len(to_block) == amount_teams_need_to_blocked:
                    return list(set(team_possible_for_slots) - set(to_block))
        return list(set(team_possible_for_slots) - set(to_block))[:open_team_slots]

    '''
    - try to avoid current connection by blocking a team from the connection
    '''
   #TODO Find more effective way to find smallest number of teams which need to be blocked to avoid the largest amount of connections
    def get_team_to_block_to_avoiding_current_connection(self, connection_string, to_block):
        connection = get_connection_from_string(connection_string)
        if connection[0] not in self.merged_match_teams and connection[1] not in self.merged_match_teams:
            if connection[1] not in to_block:
                return connection[0]
        elif connection[0] in self.merged_match_teams:
            return connection[1]
        elif connection[1] in self.merged_match_teams:
            return connection[0]

    '''
    - generates the distribution of the connection which are included in the match - when looking at the global distribution of connections
    - the values are used as a quality indicator for merging option 
    '''
    #TODO here a better metrics can be found to find the best match
    def get_quality_of_merge_option(self):
        distribution_of_connections = get_distribution_of_match(self.merged_match,
                                                                self.connections_with_global_activity)
        if self.merge_method == 'less_high':
            return list(reversed(list(distribution_of_connections.values())))
        elif self.merge_method == 'more_low':
            return list(map(lambda value: -value, list(distribution_of_connections.values())))

