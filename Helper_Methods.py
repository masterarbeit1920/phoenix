import itertools, ast, numpy, functools

'''
- generates all combinations with the 'length' for the given 'array
'''
def combinationsFromArray(array, length):
    return list(map(lambda element: sorted(list(element)), list(itertools.combinations(array, length))))

'''
- get all connections of an array of matches flattened
- connections can be converted to 'string' with 'asString' paramter
'''
def get_connections_of_combination_of_matches(matches, asString=False):
    connections = []
    for match in matches:
        connections += combinationsFromArray(match, 2)
    if asString:
        return list(map(lambda connection: str(sorted(connection)), connections))
    else:
        return connections

'''
- get all connections of a match
- connections can be converted to 'string' with 'asString' paramter
'''
def get_connections_of_match(match, asString=False):
    connections = combinationsFromArray(match, 2)
    if asString:
        return list(map(lambda connection: str(sorted(connection)), connections))
    else:
        return connections

'''
- convert a string connection back to its original from - a list
'''
def get_connection_from_string(string):
    return ast.literal_eval(string)

'''
- sort all connections of an array of matches by there appearance amount
- the keys of the dict are the times a connection appears
- the values are the actual connection as string 
'''
def get_sorted_connections_by_activity(matches):
    connections = get_connections_of_combination_of_matches(matches, asString=True)
    unique, counts = numpy.unique(connections, return_counts=True)

    sorted_ = {}
    for i in range(numpy.array(counts).min(), numpy.array(counts).max() + 1):
        sorted_[i] = []

    for i in range(len(unique)):
        sorted_[counts[i]] += [unique[i]]

    return sorted_

'''
- creates a distribution of the connection of a match in perspective of the appearing connection over all
- for each connection the appearance amount is evaluated and put together in a dictionary
- the keys are the amounts a connection appears overall in all matches
- the values are the amount of connection which are appear in the current match 
'''
def get_distribution_of_match(match, connections_with_activity):
    match_connection_activities = list(
        map(lambda connection: connections_with_activity[connection], get_connections_of_match(match, asString=True)))
    return fill_dict_with_empty_values(get_counted_array_as_dict(match_connection_activities),
                                       max(connections_with_activity.values()))

'''
- filles a dictionary with emtpy values where the original dict has a entry missing
'''
def fill_dict_with_empty_values(dict_, max_value):
    new_dict = {}
    for i in range(max_value + 1):
        if i not in dict_:
            new_dict[i] = 0
        else:
            new_dict[i] = dict_[i]
    return new_dict

'''
- count the element of an array and create a corresponding histogramm dictionary
'''
def get_counted_array_as_dict(array):
    dict_ = {}
    unique, counts = numpy.unique(array, return_counts=True)
    for i in range(len(unique)):
        dict_[unique[i]] = counts[i]
    return dict_

'''
- counts how often every connection appears overall in all matches
'''
def get_counted_connection_as_dict(matches):
    connections = get_connections_of_combination_of_matches(matches, asString=True)
    return get_counted_array_as_dict(connections)

'''
- get the connection of a match which appear in all matches together a certrain amount of time
'''
def get_connections_with_given_activity(all_connections_by_activity, match, activity, asString=False):
    if activity in all_connections_by_activity:
        connections_of_interest = all_connections_by_activity[activity]
        match_connections = get_connections_of_match(match, asString=True)
        match_connections_of_interest_str = list(set(connections_of_interest) & set(match_connections))
        if asString:
            return match_connections_of_interest_str
        else:
            return list(map(lambda connection: get_connection_from_string(connection), match_connections_of_interest_str))
    else:
        return []


def get_participation_distribution(matches):
    teams = functools.reduce(lambda a, b: a + b, matches)
    team_participation = list(get_counted_array_as_dict(teams).values())
    participation_distribution = get_counted_array_as_dict(team_participation)
    return participation_distribution


def print_connection_participation(matches):
    connections = combinationsFromArray(matches, 2)
    teams_in_connections = functools.reduce(lambda a, b: a + b, connections)
    print(get_counted_array_as_dict(teams_in_connections))
